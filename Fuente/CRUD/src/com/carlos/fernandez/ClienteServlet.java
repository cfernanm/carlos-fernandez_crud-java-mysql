package com.carlos.fernandez;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.carlos.fernandez.model.Cliente;
import com.carlos.fernandez.service.ClientService;
import com.carlos.fernandez.util.Configuration;
import com.carlos.fernandez.util.Function;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class Login
 */
@WebServlet("/cliente")
public class ClienteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ClientService clientService;
	private Gson gson = new GsonBuilder().create();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClienteServlet() {
		super();
		clientService = new ClientService();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = (HttpSession) request.getSession();
		String username = (String) session.getAttribute("username");
		// session.setAttribute("username",null);
		if (username == null) {
			response.sendRedirect(Configuration.SRV_LOGIN);

		} else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(Configuration.JSP_CLIENTE);
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

		String action = Function.getParameterString(request, "action");

		switch (action) {
		case "logout":
			logout(request, response);
			break;
		case "cargarClientes":
			cargarClientes(request, response);
			break;
		case "guardarCliente":
			guardarCliente(request, response);
			break;
		case "eliminarCliente":
			eliminarCliente(request, response);
			break;
		default:
			break;
		}

	}

	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = (HttpSession) request.getSession();
		session.setAttribute("username", null);
		session.invalidate();
		response.sendRedirect(Configuration.SRV_LOGIN);
	}

	private void cargarClientes(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String resultado = "";
		Map<String, Object> retorno = clientService.consultarClientes();
		PrintWriter out = response.getWriter();
		resultado = gson.toJson(retorno);
		out.print(resultado);
		out.flush();
	}

	private void guardarCliente(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String resultado = "";
		String nombres = Function.getParameterString(request, "nombres");
		String apellidos = Function.getParameterString(request, "apellidos");
		String _edad = Function.getParameterString(request, "edad");
		String _id = Function.getParameterString(request, "id");
		Integer edad = Function.isNumeric(_edad) ? Integer.parseInt(_edad) : 0;
		Integer id = Function.isNumeric(_id) ? Integer.parseInt(_id) : null;
		Map<String, Object> retorno = new HashMap<>();

		if (nombres != null && !nombres.isEmpty() && apellidos != null && !apellidos.isEmpty()) {
			if (id != null) {
				retorno = clientService.editarCliente(id, nombres, apellidos, edad);
			} else {
				retorno = clientService.guardarCliente(nombres, apellidos, edad);
			}
		} else {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Nombres y Apellidos son obligatorios");
		}
		PrintWriter out = response.getWriter();
		resultado = gson.toJson(retorno);
		out.print(resultado);
		out.flush();
	}

	private void eliminarCliente(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String resultado = "";
		String _id = Function.getParameterString(request, "id");
		Integer id = Function.isNumeric(_id) ? Integer.parseInt(_id) : null;
		Map<String, Object> retorno = new HashMap<>();
		if (id != null) {
			retorno = clientService.eliminarCliente(id);
		} else {
			retorno.put("codigoError", -1);
			retorno.put("mensajeUsuario", "Cliente sin ID");
		}
		PrintWriter out = response.getWriter();
		resultado = gson.toJson(retorno);
		out.print(resultado);
		out.flush();
	}

}
