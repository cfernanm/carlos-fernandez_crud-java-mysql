package com.carlos.fernandez;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.carlos.fernandez.service.ClientService;
import com.carlos.fernandez.util.Configuration;
import com.carlos.fernandez.util.Function;

/**
 * Servlet implementation class Login
 */
@WebServlet("/principal")
public class PrincipalServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PrincipalServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = (HttpSession) request.getSession();
		String username = (String) session.getAttribute("username");
		//session.setAttribute("username",null);
		if (username == null) {
			response.sendRedirect(Configuration.SRV_LOGIN);
			
		} else {
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(Configuration.JSP_PRINCIPAL);
			dispatcher.forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);
		
		String action = Function.getParameterString(request, "action");
		
		switch (action) {
		case "logout":
			logout(request, response);
			break;

		default:
			break;
		}
		

	}
	
	private void logout(HttpServletRequest request, HttpServletResponse response) throws IOException
	{
		HttpSession session = (HttpSession) request.getSession();
		session.setAttribute("username", null);
		session.invalidate();
		response.sendRedirect(Configuration.SRV_LOGIN);
	}

	

}
