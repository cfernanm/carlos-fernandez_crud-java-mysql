package com.carlos.fernandez.dao;

import java.sql.Connection;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

public class MysqlConection {
	private static PoolProperties p = null;
	private static DataSource ds = null;

	public static Connection getConnection() {
		Connection conn = null;
		try {
			if (p == null) {
				String urlDB = "jdbc:mysql://localhost:3306/";
				String userDB = "root";
				String claveDB = "admin";
				p = new PoolProperties();
				p.setUrl(urlDB);
				p.setDriverClassName("com.mysql.cj.jdbc.Driver");
				p.setUsername(userDB);
				p.setPassword(claveDB);
				p.setTestOnBorrow(true);				
				p.setInitialSize(5);
				p.setMaxActive(10);
				p.setMaxIdle(10);
				p.setLogAbandoned(true);
				p.setAlternateUsernameAllowed(true);
				ds = new DataSource();
				ds.setPoolProperties(p);
			}

			return ds.getConnection();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

}
