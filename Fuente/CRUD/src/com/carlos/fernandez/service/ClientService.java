package com.carlos.fernandez.service;

import java.util.HashMap;
import java.util.Map;

import com.carlos.fernandez.dao.ClienteDAO;

public class ClientService {
	ClienteDAO clienteDao;

	public ClientService() {
		clienteDao = new ClienteDAO();
	}

	public Map<String, Object> login(String user, String password) {
		Map<String, Object> ret = new HashMap<>();
		ret.put("codigoError", 0);
		ret.put("mensajeUsuario", "Transaccion exitosa");
		return ret;
	}

	public Map<String, Object> consultarClientes() {
		return clienteDao.consultarClientes();
	}

	public Map<String, Object> consultarCliente(int id) {
		return clienteDao.consultarCliente(id);
	}
	
	public Map<String, Object> guardarCliente(String nombres, String apellidos, Integer edad) {
		return clienteDao.guardarCliente(nombres, apellidos, edad);
	}
	
	public Map<String, Object> editarCliente(Integer id,String nombres, String apellidos, Integer edad) {
		return clienteDao.editarCliente(id,nombres, apellidos, edad);
	}
	
	public Map<String, Object> eliminarCliente(Integer id) {
		return clienteDao.eliminarCliente(id);
	}

}
