package com.carlos.fernandez.util;

import javax.servlet.http.HttpServletRequest;

public class Configuration {
	
	public static final String JSP_LOGIN = "/jsp/login.jsp";
	public static final String JSP_PRINCIPAL = "/jsp/principal.jsp";
	public static final String JSP_CLIENTE = "/jsp/cliente.jsp";
	public static final String JSP_MENU = "/jsp/menu.jsp";
	
	public static final String SRV_LOGIN = "login";
	public static final String SRV_PRINCIPAL = "principal";
	public static final String SRV_CLIENTE = "cliente";

}
