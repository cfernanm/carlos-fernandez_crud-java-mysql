package com.carlos.fernandez.util;

import javax.servlet.http.HttpServletRequest;

public class Function {

	public static String getParameterString(HttpServletRequest request, String key) {
		String retorno = request.getParameter(key);
		retorno = retorno == null ? "" : retorno;
		return retorno;
	}

	public static boolean isNumeric(String key) {
		if (key != null && !key.isEmpty()) {
			for (int i = 0; i < key.length(); i++) {
				if (!Character.isDigit(key.charAt(i))) {
					return false;
				}
			}
		}
		else
			return false;
		return true;
	}

}
