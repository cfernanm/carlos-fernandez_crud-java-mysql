package com.carlos.fernandez.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class Propertie {
	private static String BUNDLE_NAME = "config"; //$NON-NLS-1$
	private static ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	private static String ARCHIVO = "config";
	public static String RUTA=null;

	public Propertie() {
	}

	public static String getString(String key) {
		try {
			return RESOURCE_BUNDLE.getString(key);
		} catch (MissingResourceException e) {
			e.printStackTrace();
			return "true";
		}
	}	

}
