<%@page import="com.carlos.fernandez.util.Configuration"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="robots" content="noindex, nofollow">
<!-- Hojas de Estilos-->
<!-- bootstrap-4.2.1 CSS -->
<link rel="stylesheet" type="text/css"
	href="libraries/bootstrap-4.2.1/css/bootstrap.min.css">
<title>PRINCIPAL</title>
</head>
<body>
	<div class="container">
		<input type="hidden" id="csrfPreventionSalt" name="csrfPreventionSalt"
			value="${csrfPreventionSalt}" />
		<jsp:include page="<%=Configuration.JSP_MENU%>"></jsp:include>
		<div class="row">
			<div class="col-12 text-center">
			<H1>PRINCIPAL</H1>	
			</div>
		</div>
	</div>









	<!-- jquery-3.5.1 JS-->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/jquery-3.5.1/jquery.min.js"></script>
	<!-- bootstrap-4.2.1 JS  -->
	<script type="text/javascript" charset="UTF-8"
		src="libraries/bootstrap-4.2.1/js/bootstrap.min.js"></script>
</body>
</html>